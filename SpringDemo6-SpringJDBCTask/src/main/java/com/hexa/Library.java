package com.hexa;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class Library {

	// First JDBC template -  jdbcTemplate a normal variable and JdbcTemplate is a reference 
		// we are creating getter and setter for this variable
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	// method to insert the data into the table
	// Before this we have to create a table in the mysql workbench
	public int insertbook(Book b){
		String insertBTL = "insert into book values('"+b.getBook_id()+"','"+b.getBook_name()+"','"+b.getBook_author()+"')";
		return jdbcTemplate.update(insertBTL);
	}
	
	//to delete book
	public int deletebook(Book b){
		String deleteBFL = "Delete from Book where book_id='"+b.getBook_id()+"'";
		 return jdbcTemplate.update(deleteBFL);
	}
	
	// to retrieve book information based on id 
	public Book retrieveonebook(Book b){
		String retrieveoneBTL = "select * from Book where book_id='"+b.getBook_id()+"'";
		return jdbcTemplate.queryForObject(retrieveoneBTL, BeanPropertyRowMapper.newInstance(Book.class));
	}
	
	// to retrieve all book info
	public List<Book> retrieveallbook(Book b){
		String retrieveallBTL = "select * from Book";
		return jdbcTemplate.query(retrieveallBTL, BeanPropertyRowMapper.newInstance(Book.class));
	}
	
}
