package com.hexa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App 
{
    public static void main( String[] args )
    { 
    	Book b1 = new Book(211,"Homer","Illiad");
    	Book b2 = new Book(190,null,null);
    	Book b3 = new Book(1155,null,null);
    	Book b4 = new Book();
        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
       
        Library lb = (Library) context.getBean("bk");
      
        System.out.println("The book inserted into the library is :" ); 
        System.out.println( lb.insertbook(b1));
        
        System.out.println("The book removed from library is :" ); 
        System.out.println(lb.deletebook(b2));
        
        System.out.println("The detail of the book based on id in the library is :" );
        System.out.println(lb.retrieveonebook(b3));
        
        System.out.println("The all book details in the library are:");
        System.out.print(lb.retrieveallbook(b4));
        
    }
}
