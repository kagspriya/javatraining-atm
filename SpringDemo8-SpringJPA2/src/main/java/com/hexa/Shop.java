package com.hexa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Shop {
@Id
private int sid;
private String shopName;
private String type;

public void setShopName(String shopName) {
	this.shopName = shopName;
}
public String getShopName() {
	return shopName;
}
public void setSid(int sid) {
	this.sid = sid;
}
public int getSid() {
	return sid;
}
public void setType(String type) {
	this.type = type;
}
public String getType() {
	return type;
}

}