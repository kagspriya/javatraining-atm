package com.hexa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App 
{
    public static void main( String[] args )
    {
    	Shop e = new Shop();
     	e.setSid(103);
     	e.setShopName("Batta");
e.setType("Footwear Shop");

     	
     	EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
     	EntityManager em = emf.createEntityManager();
        
     	em.getTransaction().begin();
     	em.persist(e);
     	em.getTransaction().commit();
     	Shop s = em.find(Shop.class,103);
     	System.out.println( "Shop Detail : "+s.getSid()+"  "+s.getShopName()+"  "+s.getType() );

    }
}
