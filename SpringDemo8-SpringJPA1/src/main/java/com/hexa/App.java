package com.hexa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App 
{
    public static void main( String[] args )
    {
    	Books b = new Books();
    	b.setBid(185);
    	b.setBname("Pride and Prejudice ");
    	b.setBauthor(" Jane Austen");
    	
    	// service class 
    	EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu");
     	EntityManager em = emf.createEntityManager();
     	em.getTransaction().begin();
     	em.persist(b);
     	em.getTransaction().commit();
     
     	Books bk = em.find(Books.class,185);
     	System.out.println( "Books Detail : "+bk.getBid()+"  "+bk.getBname()+"  "+bk.getBauthor());

    }
}
